<?php
require_once 'api/mmClientConfig.php';
require_once 'vendor/autoload.php';

$tokenStorage = new fkooman\OAuth\Client\SessionStorage();
$httpClient = new Guzzle\Http\Client();

$mmClientConfig = new mmClientConfig("currency");

$clientConfig = $mmClientConfig->GetClientConfig();
$originalCallerUrl =$_SESSION['mmApiSampleMethodUrl']; 

try {
    $httpClient->setSslVerification(false, false, 2);
    $cb = new fkooman\OAuth\Client\Callback("foo", $clientConfig, $tokenStorage, $httpClient);
    $cb->handleCallback($_GET);

    header("HTTP/1.1 302 Found");
    header("Location: ".$originalCallerUrl);
    exit;
}
catch (fkooman\OAuth\Client\Exception\AuthorizeException $e) {
    // this exception is thrown by Callback when the OAuth server returns a
    // specific error message for the client, e.g.: the user did not authorize
    // the request
    die(sprintf("ERROR: %s, DESCRIPTION: %s", $e->getMessage(), $e->getDescription()));
}
catch (Exception $e) {
    // other error, these should never occur in the normal flow
    die(sprintf("ERROR: %s", $e->getMessage()));
}
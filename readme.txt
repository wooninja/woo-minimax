=== Minimax - Izvoz naročil iz Woocommerce ===
Contributors: mroky
Donate link: http://optininja.com/
Tags: integration, minimax, orders, export
Requires at least: 4.6
Requires PHP: 5.4
Tested up to: 4.7
Stable tag: 4.3
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Izvoz naročil preko API v Minimax

== Description ==

Hiter in enostaven izvoz naročil v format za Minimax

== Licence ==
Please note, all available code is now open source and no support is provided for installation, use or further development of these extensions. 
You’ll be able to use all the extensions without any time limit. We also give you the freedom to fix bugs, add features, and change the code as you want. 
All extensions are release under GNU General Public License, version 3 (GPL-3.0).


== Changelog ==
= 1.0 =
* Osnovna verzija izvoza
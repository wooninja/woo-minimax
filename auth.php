<?php

$params = array(
    'client_id'        => 'alphawolves',
    'client_secret'    => 'duMvd968QDkeWdaH',
    'grant_type'       => 'password',
    'username'         => 'TestWooek',
    'password'         => 'woo12345',
    'scope'            => 'minimax.si'
);

$request = array(
    'http' => array(
        'method'        => 'POST',
        'header'        => array(
            'Content-type: application/x-www-form-urlencoded',
        ),
        'content'        => http_build_query($params),
        'timeout'        => 10,
    )
);

if (!$response = file_get_contents('https://moj.minimax.si/demo/si/aut/oauth20/token', false, stream_context_create($request))) {
    die('auth error');
}

$token = json_decode($response);

$request = array(
    'http' => array(
        'method'        => 'GET',
        'header'        => 'Authorization: Bearer ' . $token->access_token,
        'timeout'        => 10,
    )
);
if (!$response = file_get_contents('https://moj.minimax.si/demo/si/api/api/currentuser/orgs', false, stream_context_create($request))) {
    die('orgs error');
}
$orgs = json_decode($response, true);
print_r($orgs);

<?php
/*
Plugin Name: WooCommerce Minimax
Plugin URI: https://optininja.com
Description: Integracija Woocommerce -> Minimax
Version: 1.0.0
Author: OptiNinja
Author Email: info@optininja.com
License: GPLv2 or later

  Copyright 2017 OptiNinja (info@optininja.com)

*/
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

define( 'MM_VERSION', '1.0.0' );

define( 'MM_PLUGIN', __FILE__ );

define( 'MM_PLUGIN_BASENAME', plugin_basename( MM_PLUGIN ) );

define( 'MM_PLUGIN_NAME', trim( dirname( MM_PLUGIN_BASENAME ), '/' ) );

define( 'MM_PLUGIN_DIR', untrailingslashit( dirname( MM_PLUGIN ) ) );

define( 'MM_PLUGIN_URL', plugin_dir_url( __FILE__ ) );



	function printer( $podatki ) {
		echo "<pre>";
		print_r($podatki);
		echo "</pre>";
	}

	/**
	 * Administration menu
	 */
	add_action( 'admin_menu', 'mm_administration', 9 );

	function mm_administration() {
		add_menu_page( __( 
			'Minimax', 'mm-sm' ),
			__( 'Minimax', 'mm-sm' ),
			'manage_options', 'mm-module',
			'mm_settings', 'dashicons-feedback' );

		$settings = add_submenu_page( 
			'mm-module',
			__( 'Nastavitve', 'mm-sm' ),
			__( 'Minimax', 'mm-sm' ),
			'manage_options', 'mm-module',
			'mm_settings' );
	}


	function mm_settings() {
		include("admin/settings.php");
	}

	/**
	 * Saves the custom meta input
	 */
	function mm_meta_save( $post_id ) {
	 
	    // Checks save status
	    $is_autosave = wp_is_post_autosave( $post_id );
	    $is_revision = wp_is_post_revision( $post_id );
	 
	    // Exits script depending on save status
	    if ( $is_autosave || $is_revision ) {
	        return;
	    }
	 
	    if( isset( $_POST[ 'dobavitelj' ] ) ) {
	        update_post_meta( $post_id, 'minimaxDobavitelj', sanitize_text_field( $_POST[ 'dobavitelj' ] ) );
	    }	 
	    if( isset( $_POST[ 'skladisce' ] ) ) {
	        update_post_meta( $post_id, 'minimaxSkladisce', sanitize_text_field( $_POST[ 'skladisce' ] ) );
	    }	    
		if( isset( $_POST[ 'syncZalogo' ] ) ) {
		    update_post_meta( $post_id, 'minimaxAutomaticZaloga', 'yes' );
		} else {
		    update_post_meta( $post_id, 'minimaxAutomaticZaloga', '' );
		}

	}
	add_action( 'save_post', 'mm_meta_save' );

	/**
	 * Add the meta box on the product page
	 */
	function mm_meta_box_product() {
		add_meta_box( 'mm-box-zaloga', __( 'Zaloga - Minimax', 'mm-sm' ), 'mm_box_content_product', 'product', 'normal', 'high' );
	}

	/**
	 * Create the meta box content on the single product page
	 */
	function mm_box_content_product() {
		global $post_id;

		$product 	= new WC_Product( $post_id );
		$synced = get_post_meta( $product->id, "minimaxSync", true );

		if ( $synced == "" ) {
			echo "Artikel ne obstaja v Minimaxu.";
			return true;
		}

		////////////////////////////////////////////////////
		////////////////////////////////////////////////////

		$sync       	= get_post_meta( $post_id, "minimaxAutomaticZaloga", true );
		$skladisce  	= get_post_meta( $post_id, "minimaxSkladisce", true );
		$dobavitelj 	= get_post_meta( $post_id, "minimaxDobavitelj", true );
		$zaloga 		= get_post_meta( $post_id, "minimaxStock", true );
		if ( $zaloga == "" ) {
			$zaloga = 0;
		}
		$stranke 	= get_option("minimaxCustomers", array() );
		$skladisca 	= get_option("minimaxWarehouses", array() );

		////////////////////////////////////////////////////
		////////////////////////////////////////////////////

		///printer( $skladisca );
	?>
		<input type="hidden" value="<?php echo $post_id;?>" class="id-produkta">

		<p>
			Zaloga za ta produkt v Minimaxu: <strong><?php echo $zaloga;?></strong>
		</p>
		<p>
		    <label class="prfx-row-title"><?php _e( 'Dobavitelj:', 'mm-sm' )?></label>
		    <select name="dobavitelj">
		    	<option value="">/</option>
		    	<?php foreach ($stranke as $key => $value) {
		    	?>
					<option <?php if( $value["CustomerId"] == $dobavitelj ) { echo "selected='selected' "; }; ?>value="<?php echo $value["CustomerId"];?>"><?php echo $value["Name"];?> | <?php echo $value["Address"];?></option>
		    	<?php
		    	}
		    	?>
		    </select>
		</p>
		<p>
		    <label class="prfx-row-title"><?php _e( 'Skladišče:', 'mm-sm' )?></label>
		    <select name="skladisce">
		    	<option value="">/</option>
		    	<?php foreach ($skladisca as $key => $value) {
		    	?>
					<option <?php if( $value["WarehouseId"] == $skladisce ) { echo "selected='selected' "; }; ?>value="<?php echo $value["WarehouseId"];?>"><?php echo $value["Name"];?></option>
		    	<?php
		    	}
		    	?>
		    </select>
		</p>
		<p>
		    <label class="prfx-row-title"><?php _e( 'Samodejno posodabljaj zalogo iz Minimaxa:', 'mm-sm' )?></label>
		    <input type="checkbox" value="1" name="syncZalogo" <?php if( $sync != "" ) { echo "checked='checked' "; }; ?>>
		</p>
		<hr>
		<h4>Prejem zaloge:</h4>

		<p>
		    <label class="prfx-row-title"><?php _e( 'Število nove prejete zaloge:', 'mm-sm' )?></label>
		    <input type="text" placeholder="0" class="js-nova-zaloga" name="nova-zaloga">
		</p>
		<p>
			<a href="" class="button js-add-new-stock">Dodaj nov prejem zaloge</a>
		</p>

	<?php
	}

	///////////////////////////////////////////////////////////////

	/**
	 * Add the meta box on the single order page
	 */
	function mm_meta_box() {
		add_meta_box( 'mm-box', __( 'Minimax', 'mm-sm' ), 'mm_box_content', 'shop_order', 'side', 'low' );
	}

	/**
	 * Create the meta box content on the single order page
	 */
	function mm_box_content() {
		global $post_id;

		$order = new WC_Order( $post_id );

		$invoice = get_post_meta( $post_id, "minimaxInvoice", true );
		$year    = get_post_meta( $post_id, "minimaxNumberYear", true );
		$number  = get_post_meta( $post_id, "minimaxNumber", true );

		/// Get Invoice
		if ( $invoice != "" && $year == "" ) {
		//if ( $invoice != "" ) {

			include_once MM_PLUGIN_DIR . '/api/api.php';
			$organizacija 	= get_option("organisationId", true);

			$api = new MMApi( 
				get_option("apiClientId", true),
				get_option("apiClientSecret", true),
				get_option("clientContextUserId", true),
				get_option("clientContextUserPass", true),
				get_option("apiRootUrl", true)
			);

			$invoice_data = $api->getInvoice( $organizacija, $invoice );

			update_post_meta( $post_id, "minimaxNumberYear", $invoice_data->Year );
			update_post_meta( $post_id, "minimaxNumber", $invoice_data->InvoiceNumber );

			$year    = get_post_meta( $post_id, "minimaxNumberYear", true );
			$number  = get_post_meta( $post_id, "minimaxNumber", true );


		}

	?>

		<div class="print-actions minimax-spletni-moduli">

			
			<?php
			if ($invoice == "") {
			?>
				<a href="<?php echo wp_nonce_url( admin_url( 'admin-ajax.php?action=minimax_invoice&spletnimoduli=true&order='.$post_id ), 'sync_products' );?>" class="button">Izdaj račun v Minimax</a>
			<?php
			} else {
			?>
				Številka računa: <br>
				<strong><?php echo $year;?>-<?php echo $number;?></strong>
			<?php
			}
			?>




		</div>

	<?php

	}


	add_action( 'admin_init', 'mm_load_admin_hooks' );

	/**
	 * Load the admin hooks
	 */
	function mm_load_admin_hooks() {

	   // Hooks
	   add_action( 'add_meta_boxes_product', 'mm_meta_box_product' );
	   add_action( 'add_meta_boxes_shop_order', 'mm_meta_box' );
       wp_register_style( 'mm_wp_admin_css', plugin_dir_url( __FILE__ ) . 'admin/styles/spletni-moduli-minimax.css', false, MM_VERSION );
       wp_enqueue_style( 'mm_wp_admin_css' );

	}

	register_activation_hook( __FILE__, 'wi_sync_zaloga_schedule' );
	function wi_sync_zaloga_schedule(){
	  //Use wp_next_scheduled to check if the event is already scheduled
	  $timestamp = wp_next_scheduled( 'wi_sync_zaloga' );

	  //If $timestamp == false schedule daily backups since it hasn't been done previously
	  if( $timestamp == false ){
	    //Schedule the event for right now, then to repeat daily using the hook 'wi_create_daily_backup'
	    wp_schedule_event( time(), 'hourly', 'wi_sync_zaloga' );
	  }
	}

	//Hook our function , wi_create_backup(), into the action wi_create_daily_backup
	add_action( 'wi_sync_zaloga', 'sync_minimax_zalogo' );



	add_action( 'wp_ajax_sync_minimax_zalogo', 'sync_minimax_zalogo' );
	function sync_minimax_zalogo() {


		global $wpdb;

		include_once MM_PLUGIN_DIR . '/api/api.php';
		$organizacija 	= get_option("organisationId", true);

		if ( $organizacija == "" ) {
			return;
		}

		$api = new MMApi( 
			get_option("apiClientId", true),
			get_option("apiClientSecret", true),
			get_option("clientContextUserId", true),
			get_option("clientContextUserPass", true),
			get_option("apiRootUrl", true)
		);

		$stocks =  $api->getAllStocks( $organizacija );

		if ( !isset( $stocks["Rows"] ) ) {
			return false;
		}

		$produkti = get_woocommerce_product_list();

		foreach ($produkti as $key => $value) {

			if ( $value->synced == FALSE ) {
				continue;
			}

			single_product_zaloga_minimax( $value->id, $stocks["Rows"] );

		}

	}

	function single_product_zaloga_minimax( $ID,  $stocks ) {

		$product    = new WC_Product( $ID );
		$minimax    = get_post_meta( $ID , "minimaxID", true );
		$sync       = get_post_meta( $ID , "minimaxAutomaticZaloga", true );

		if ( $minimax == "" ) {
			return false;
		}
		if ( $sync == "" ) {
			return false;
		}


		foreach ($stocks as $key => $value) {
			
			if ( $value["Item"]["ID"] == $minimax ) {
				
				//echo "Found!";
				//printer( $value );
				update_post_meta( $ID, "minimaxStock", $value["Quantity"] );
				$product->set_stock( $value["Quantity"] );
				break;

			}

		}

		

	}
	
	add_action( 'wp_ajax_single_product_add_stock', 'single_product_add_stock' );

	function single_product_add_stock( ) {

		$data = array();

		$data["status"] = "error";

		global $wpdb;

		include_once MM_PLUGIN_DIR . '/api/api.php';
		$organizacija 	= get_option("organisationId", true);

		$api = new MMApi( 
			get_option("apiClientId", true),
			get_option("apiClientSecret", true),
			get_option("clientContextUserId", true),
			get_option("clientContextUserPass", true),
			get_option("apiRootUrl", true)
		);


		$produkt = new WC_Product( $_POST["id"] );
		$theid = $_POST["id"];


		$synced 		= get_post_meta( $produkt->id, "minimaxSync", true );
		$minimaxID      = get_post_meta( $produkt->id, "minimaxID", true );
		$sync       	= get_post_meta( $theid, "minimaxAutomaticZaloga", true );
		$skladisce  	= get_post_meta( $theid, "minimaxSkladisce", true );
		$dobavitelj 	= get_post_meta( $theid, "minimaxDobavitelj", true );

		$error = false;
		$msg = "";

		if ( $minimaxID == "" ) {
			$msg += "Artikel ne obstaja v Minimaxu.";
			$error = true;
		}

		if ( $skladisce == "" ) {
			$msg += "Manjka podatek o skladišču.";
			$error = true;
		}

		if ( $dobavitelj == "" ) {
			$msg += "Manjka podatek o dobavitelju.";
			$error = true;
		}


			if ( $error == TRUE ) {
				$data["explanation"] = $msg;
				echo json_encode($data);
				wp_die();
				return false;
			}


			$order = array();


			$order["StockEntryType"] = "P";
			$order["StockEntrySubtype"] = "S";
			$order["Status"] = "P";
			$order["Number"] = null;
			$order["Date"] = date("Y-m-d H:i:s");
			$order["Customer"] = array(
				"ID" => $dobavitelj,
				"Name" => null,
				"ResourceUrl" => null,
			);
			$order["Analytic"] = null;
			$order["Currency"] = array(
				"ID" => get_option( "valutaTrgovine", true ),
				"Name" => null,
				"ResourceUrl" => null,
			);



			$order["StockEntryRows"] = array();



			$c = 1;

			$order["StockEntryRows"][] = array(
				"StockEntryRowId" =>  null,
				"StockEntry" =>  null,
				"Item" =>  array(
					"ID" => $minimaxID,
					"Name" => null,
					"ResourceUrl" => null,
				),
				"RowNumber"  => $c,
				"ItemName" =>  $produkt->post->post_title,
				"ItemCode" =>  null,
				"Description" =>  null,
				"WarehouseTo" => array(
					"ID" => $skladisce
				),
				"Quantity" =>  $_POST["qty"],
				"Price" =>  $produkt->get_price(),
				"RecordDtModified" =>  "0001-01-01T00:00:00",
				"RowVersion" =>  null,
				"_links" =>  null
			);



			$order["RecordDtModified"] = "0001-01-01T00:00:00";
			$order["RowVersion"] = null;
			$order["_links"] = null;



		$stock = $api->addStockEntry( $organizacija, json_encode($order) );
		if ( $stock != "ERROR" ) {
			$data["status"] = "success";
		} else {
			$data["explanation"] = $msg;
		}

		echo json_encode($data);

		wp_die();

	}


	add_action( 'wp_ajax_minimax_invoice', 'poslji_minimax' );

	function poslji_minimax( ) {
		global $wpdb;

		include_once MM_PLUGIN_DIR . '/api/api.php';
		$organizacija 	= get_option("organisationId", true);

		$api = new MMApi( 
			get_option("apiClientId", true),
			get_option("apiClientSecret", true),
			get_option("clientContextUserId", true),
			get_option("clientContextUserPass", true),
			get_option("apiRootUrl", true)
		);

		$order_id = $_GET["order"];
		$nakup = new WC_Order( $order_id );
		$address = $nakup->get_address();


		/// Že imamo ID stranke?
		$stranka = get_post_meta( $order_id, "minimaxCustomer", true );

		if ( $stranka == "" ) {
			/// Nimamo še stranke
			$stranka = '{
			"CustomerId": 0,
			"Code": null,
			"Name": "'.$nakup->get_formatted_billing_full_name().'",
			"Address": "'.$address["address_1"].'",
			"PostalCode": "'.$address["postcode"].'",
			"City": "'.$address["city"].'",
			"Country": {
			    "ID": 192,
			    "Name": null,
			    "ResourceUrl": null
			},
			"CountryName": null,
			"TaxNumber": null,
			"RegistrationNumber": null,
			"VATIdentificationNumber": null,
			"SubjectToVAT": "N",
			"Currency": {
			    "ID": '.get_option( "valutaTrgovine", true ).',
			    "Name": null,
			    "ResourceUrl": null
			},
			"ExpirationDays": 0,
			"RebatePercent": 0.0,
			"WebSiteURL": null,
			"EInvoiceIssuing": "N",
			"InternalCustomerNumber": null,
			"Usage": null,
			"RecordDtModified": "0001-01-01T00:00:00",
			"RowVersion": null
			}';

			$stranka = $api->addCustomer( $organizacija, $stranka );
			update_post_meta( $nakup->id, "minimaxCustomer", $stranka );
		}


		/////////////////////////////////////////////////////////////////////////////////

		$order = array();

		$order["InvoiceType"] = "R";
		$order["Status"] = "I";
		$order["IssuedInvoiceId"] = null;
		$order["Year"] = date("Y", strtotime($nakup->post->post_date));
		$order["InvoiceNumber"] = null;
		$order["Numbering"] = null;
		$order["DocumentNumbering"] = null;
		$order["Date"] = $nakup->post->post_date;
		$order["Customer"] = array(
			"ID" => $stranka,
			"Name" => null,
			"ResourceUrl" => null,
		);
		$order["DateIssued"] = date("Y-m-d H:i:s");
		$order["DateTransaction"] = date("Y-m-d H:i:s");
		$order["DateDue"] = date("Y-m-d H:i:s");
		$order["AddresseeName"] = $nakup->get_formatted_billing_full_name();
		$order["AddresseeAddress"] = $address["address_1"];
		$order["AddresseePostalCode"] = $address["postcode"];
		$order["AddresseeCity"] = $address["city"];
		$order["AddresseeCountry"] = array(
			"ID" => 192,
			"Name" => null,
			"ResourceUrl" => null,
		);
		$order["AddresseeCountryName"] = null;
		$order["RecipientName"] = null;
		$order["RecipientAddress"] = null;
		$order["RecipientPostalCode"] = null;
		$order["RecipientCountryName"] = null;
		$order["AddresseeCountryName"] = null;
		$order["RecipientCountry"] = null;

		$order["Analytic"] = null;
		$order["Currency"] = array(
			"ID" => get_option( "valutaTrgovine", true ),
			"Name" => null,
			"ResourceUrl" => null,
		);
		$order["PricesOnInvoice"] = "N";
		$order["RecurringInvoice"] = "N";

		$order["DescriptionAbove"] = null;
		$order["DescriptionBelow"] = null;
		$order["IssuedInvoiceReportTemplate"] = array(
			"ID" => get_option( "reportInvoice", true ),
			"Name" => null,
			"ResourceUrl" => null,
		);
		$order["DeliveryNoteReportTemplate"] = array(
			"ID" => get_option( "DeliveryNoteReportTemplate", true ),
			"Name" => null,
			"ResourceUrl" => null,
		);

		$order["OrderRows"] = array();


		$rows = $nakup->get_items( array( "line_item" ) );

		$c = 1;
		foreach ($rows as $key => $value) {

			$produkt = new WC_Product( $value["product_id"] );

			/// Nimamo davka na tem produktu
			if ( (float)$value["line_subtotal_tax"] == 0 ) {
				$tax = 0;
			} else {
				$tax_rate = $value["line_subtotal_tax"] / $value["line_subtotal"] * 100;
				$tax = $tax_rate;
			}


			$davek = findVAT( $tax );


			$minimaxID = get_post_meta( $value["product_id"] , "minimaxID", true );
			$order["IssuedInvoiceRows"][] = array(
				"IssuedInvoiceRowId" =>  null,
				"IssuedInvoice" =>  null,
				"Item" =>  array(
					"ID" => $minimaxID,
					"Name" => null,
					"ResourceUrl" => null,
				),
				"RowNumber"  => $c,
				"ItemName" =>  $value["name"],
				"ItemCode" =>  null,
				"Description" =>  null,
				"VatRate" => array(
					"ID" => $davek,
					"Name" => null,
					"ResourceUrl" => null,
				),
				"Quantity" =>  $value["qty"],
				"Price" =>  $value["line_total"],
				"PriceWithVAT" => $value["line_total"]+$value["line_subtotal_tax"],
				"VATPercent" => $tax_rate,
				"UnitOfMeasurement" =>  null,
				"RecordDtModified" =>  "0001-01-01T00:00:00",
				"RowVersion" =>  null,
				"_links" =>  null
			);


			$c++;


			//// ČE IMAMO VKLOPLJENO RAZBREMENITEV ZALOGE, MORAMO IZDATI ZALOGO V MINIMAXU
			if( get_option( "syncOrders" ) ) { 

				$sync       	= get_post_meta( $value["product_id"], "minimaxAutomaticZaloga", true );
				$skladisce  	= get_post_meta( $value["product_id"], "minimaxSkladisce", true );
				$dobavitelj 	= get_post_meta( $value["product_id"], "minimaxDobavitelj", true );

				if ( $skladisce != "" && $dobavitelj != "" ) {
					
						$stock = array();
						$stock["StockEntryType"] = "I";
						$stock["StockEntrySubtype"] = "S";
						$stock["Status"] = "P";
						$stock["Number"] = null;
						$stock["Date"] = date("Y-m-d H:i:s");
						$stock["Customer"] = array(
							"ID" => $dobavitelj,
							"Name" => null,
							"ResourceUrl" => null,
						);
						$stock["Analytic"] = null;
						$stock["Currency"] = array(
							"ID" => get_option( "valutaTrgovine", true ),
							"Name" => null,
							"ResourceUrl" => null,
						);

						$stock["AddresseeName"] = $nakup->get_formatted_billing_full_name();
						$stock["AddresseeAddress"] = $address["address_1"];
						$stock["AddresseePostalCode"] = $address["postcode"];
						$stock["AddresseeCity"] = $address["city"];
						$stock["AddresseeCountry"] = array(
							"ID" => 192,
							"Name" => null,
							"ResourceUrl" => null,
						);
						$stock["AddresseeCountryName"] = null;
						$stock["RecipientName"] = null;
						$stock["RecipientAddress"] = null;
						$stock["RecipientPostalCode"] = null;
						$stock["RecipientCountryName"] = null;
						$stock["AddresseeCountryName"] = null;
						$stock["RecipientCountry"] = null;

						$stock["StockEntryRows"] = array();



						$pp = 1;

						$stock["StockEntryRows"][] = array(
							"StockEntryRowId" =>  null,
							"StockEntry" =>  null,
							"Item" =>  array(
								"ID" => $minimaxID,
								"Name" => null,
								"ResourceUrl" => null,
							),
							"RowNumber"  => $pp,
							"ItemName" =>  $produkt->post->post_title,
							"ItemCode" =>  null,
							"Description" =>  null,
							"WarehouseFrom" => array(
								"ID" => $skladisce
							),
							"Quantity" =>  $value["qty"],
							"Price" =>  $produkt->get_price(),
							"RecordDtModified" =>  "0001-01-01T00:00:00",
							"RowVersion" =>  null,
							"_links" =>  null
						);

						$stock["RecordDtModified"] = "0001-01-01T00:00:00";
						$stock["RowVersion"] = null;
						$stock["_links"] = null;
						//printer( $stock );
						//echo json_encode($stock);
						$stock = $api->addStockEntry( $organizacija, json_encode($stock) );
						if ( $stock != "ERROR" ) {
							$s = array( "id" => $stock, "action" => "confirm" );
							$api->stockAction( $organizacija, $s );
						}
						//echo $stock;
				}

			}


		}

		$rows = $nakup->get_items( array( "shipping" ) );


		foreach ($rows as $key => $value) {


			/// Nimamo davka na tem produktu
			if ( empty( unserialize( $value["taxes"] ) ) ) {
				$tax = 0;
			} else {
				$vsidavki = unserialize($value["taxes"]);

				$tax = 0;

				foreach ($vsidavki as $d => $id) {
					$tax += $id;
				}

				$value["line_subtotal_tax"] = $tax;

				$tax_rate = $value["line_subtotal_tax"] / $value["cost"] * 100;
				$tax = $tax_rate;
			}



			$davek = findVAT( $tax );

			$order["IssuedInvoiceRows"][] = array(
				"IssuedInvoiceRowId" =>  null,
				"IssuedInvoice" =>  null,
				"Item" =>  array(
					"ID" => get_option( "minimaxPostnina" ),
					"Name" => null,
					"ResourceUrl" => null,
				),
				"RowNumber"  => $c,
				"ItemName" =>  $value["name"],
				"ItemCode" =>  null,
				"Description" =>  null,
				"VatRate" => array(
					"ID" => $davek,
					"Name" => null,
					"ResourceUrl" => null,
				),
				"Quantity" =>  1,
				"Price" =>  $value["cost"],
				"PriceWithVAT" => $value["cost"]+$value["line_subtotal_tax"],
				"VATPercent" => $tax_rate,
				"UnitOfMeasurement" =>  null,
				"RecordDtModified" =>  "0001-01-01T00:00:00",
				"RowVersion" =>  null,
				"_links" =>  null
			);


			$c++;

		}


		$order["RecordDtModified"] = "0001-01-01T00:00:00";
		$order["RowVersion"] = null;
		$order["_links"] = null;


	    $invoice = $api->addInvoice( $organizacija, json_encode( $order ) );

	    sync_minimax_zalogo();

	    if ( $invoice != "ERROR" ) {
	    	update_post_meta( $nakup->id, "minimaxInvoice", $invoice );

		    $invoiceAction = $api->invoiceAction( $organizacija, array( "id" => $invoice, "action" => "issue" ) );

		    if ( $invoiceAction != "ERROR" ) {
	    		header('Location: '.admin_url( 'post.php?post='.$order_id.'&action=edit' ));
		    } else {
		    	echo "Prišlo je do napake pri izstavljanju računa! Račun je bil kreiran v Minimax!";
		    }


	    } else {
	    	echo "Prišlo je do napake pri kreiranju računa v Minimax!";
	    }

		/////////////////////////////////////////////////////////////////////////////////


	}


	add_action( 'woocommerce_checkout_update_order_meta', 'narocilo', 100, 1 );
	function narocilo( $order_id ) {
		global $wpdb;

		if( get_option( "syncOrders" ) ) { 

			include_once MM_PLUGIN_DIR . '/api/api.php';
			$organizacija 	= get_option("organisationId", true);

			$api = new MMApi( 
				get_option("apiClientId", true),
				get_option("apiClientSecret", true),
				get_option("clientContextUserId", true),
				get_option("clientContextUserPass", true),
				get_option("apiRootUrl", true)
			);


			$nakup = new WC_Order( $order_id );

			$address = $nakup->get_address();

			$stranka = '{
			"CustomerId": 0,
			"Code": null,
			"Name": "'.$nakup->get_formatted_billing_full_name().'",
			"Address": "'.$address["address_1"].'",
			"PostalCode": "'.$address["postcode"].'",
			"City": "'.$address["city"].'",
			"Country": {
			    "ID": 192,
			    "Name": null,
			    "ResourceUrl": null
			},
			"CountryName": null,
			"TaxNumber": null,
			"RegistrationNumber": null,
			"VATIdentificationNumber": null,
			"SubjectToVAT": "N",
			"Currency": {
			    "ID": '.get_option( "valutaTrgovine", true ).',
			    "Name": null,
			    "ResourceUrl": null
			},
			"ExpirationDays": 0,
			"RebatePercent": 0.0,
			"WebSiteURL": null,
			"EInvoiceIssuing": "N",
			"InternalCustomerNumber": null,
			"Usage": null,
			"RecordDtModified": "0001-01-01T00:00:00",
			"RowVersion": null
			}';

			$stranka = $api->addCustomer( $organizacija, $stranka );


			/////////////////////////////////////////////////////////////////////////////////


			$order = array();

			$order["OrderId"] = null;
			$order["ReceivedIssued"] = "P";
			$order["Year"] = date("Y", strtotime($nakup->post->post_date));
			$order["Number"] = null;
			$order["Date"] = $nakup->post->post_date;
			$order["Customer"] = array(
				"ID" => $stranka,
				"Name" => null,
				"ResourceUrl" => null,
			);
			$order["CustomerName"] = $nakup->get_formatted_billing_full_name();
			$order["CustomerAddress"] = $address["address_1"];
			$order["CustomerPostalCode"] = $address["postcode"];
			$order["CustomerCity"] = $address["city"];

			$order["CustomerCountry"] = array(
				"ID" => 33,
				"Name" => null,
				"ResourceUrl" => null,
			);
			$order["CustomerCountryName"] = null;
			$order["Analytic"] = null;
			$order["DueDate"] = null;
			$order["Reference"] = null;

			$order["Currency"] = array(
				"ID" => get_option( "valutaTrgovine", true ),
				"Name" => null,
				"ResourceUrl" => null,
			);

			$order["Notes"] = null;
			$order["Document"] = null;
			$order["DateConfirmed"] = null;
			$order["DateCompleted"] = null;
			$order["DateCanceled"] = null;
			$order["Status"] = null;
			$order["DescriptionAbove"] = null;
			$order["DescriptionBelow"] = null;
			$order["ReportTemplate"] = array(
				"ID" => get_option( "reportOrder", true ),
				"Name" => null,
				"ResourceUrl" => null,
			);
			$order["OrderRows"] = array();


			$rows = $nakup->get_items( array( "line_item" ) );


			foreach ($rows as $key => $value) {

				$minimaxID = get_post_meta( $value["product_id"] , "minimaxID", true );

				$order["OrderRows"][] = array(
					"OrderRowId" =>  null,
					"Order" =>  null,
					"Item" =>  array(
						"ID" => $minimaxID,
						"Name" => null,
						"ResourceUrl" => null,
					),
					"ItemName" =>  $value["name"],
					"ItemCode" =>  null,
					"Description" =>  null,
					"Quantity" =>  $value["qty"],
					"Price" =>  $value["line_total"]+$value["line_subtotal_tax"],
					"UnitOfMeasurement" =>  null,
					"RecordDtModified" =>  "0001-01-01T00:00:00",
					"RowVersion" =>  null,
					"_links" =>  null
				);


			}


			$order["RecordDtModified"] = "0001-01-01T00:00:00";
			$order["RowVersion"] = null;
			$order["_links"] = null;


			$order = $api->addOrder( $organizacija, json_encode($order) );
			if ( $order != "ERROR" ) {
				update_post_meta( $nakup->id, "minimaxOrder", $order );
				update_post_meta( $nakup->id, "minimaxCustomer", $stranka );
			} else {
				echo "Prišlo je do napake!";
			}

		}

	}


	add_action( 'wp_ajax_sync_zaloga_first', 'sync_zaloga_first' );
	function sync_zaloga_first() {
		global $wpdb;

		include_once MM_PLUGIN_DIR . '/api/api.php';
		$organizacija 	= get_option("organisationId", true);


		$produkti = get_woocommerce_product_list();
		$data = array();

		?>
			<style>
				.success {
					color: green;
					font-weight: bold;
				}

				.error {
					color: red;
					font-weight: bold;
				}
			</style>
	
		<a href="<?php echo admin_url( 'admin.php?page=mm-module' );?>"> <- Nazaj</a>

		<?php

		$api = new MMApi( 
			get_option("apiClientId", true),
			get_option("apiClientSecret", true),
			get_option("clientContextUserId", true),
			get_option("clientContextUserPass", true),
			get_option("apiRootUrl", true)
		);

		foreach ($produkti as $key => $value) {

			$produkt = new WC_Product( $value->id );

			if ( $value->synced == FALSE ) {
				continue;
			}

			echo "<br> ";
			echo "<br> ";
			echo ">>>> Dodajam <strong>'".$value->post->post_title."'</strong>. ";

			$error = false;

			if ( $value->minimaxID == "" ) {
				echo "Artikel ne obstaja v Minimaxu.";
				$error = true;
			}

			if ( $value->skladisce == "" ) {
				echo "Manjka podatek o skladišču.";
				$error = true;
			}

			if ( $value->dobavitelj == "" ) {
				echo "Manjka podatek o dobavitelju.";
				$error = true;
			}





				$order = array();


				$order["StockEntryType"] = "P";
				$order["StockEntrySubtype"] = "S";
				$order["Status"] = "P";
				$order["Number"] = null;
				$order["Date"] = date("Y-m-d H:i:s");
				$order["Customer"] = array(
					"ID" => $value->dobavitelj,
					"Name" => null,
					"ResourceUrl" => null,
				);
				$order["Analytic"] = null;
				$order["Currency"] = array(
					"ID" => get_option( "valutaTrgovine", true ),
					"Name" => null,
					"ResourceUrl" => null,
				);

	

				$order["StockEntryRows"] = array();



				$c = 1;

				$order["StockEntryRows"][] = array(
					"StockEntryRowId" =>  null,
					"StockEntry" =>  null,
					"Item" =>  array(
						"ID" => $value->minimaxID,
						"Name" => null,
						"ResourceUrl" => null,
					),
					"RowNumber"  => $c,
					"ItemName" =>  $value->post->post_title,
					"ItemCode" =>  null,
					"Description" =>  null,
					"WarehouseTo" => array(
						"ID" => $value->skladisce
					),
					"Quantity" =>  $produkt->get_stock_quantity(),
					"Price" =>  $value->price,
					"RecordDtModified" =>  "0001-01-01T00:00:00",
					"RowVersion" =>  null,
					"_links" =>  null
				);



				$order["RecordDtModified"] = "0001-01-01T00:00:00";
				$order["RowVersion"] = null;
				$order["_links"] = null;


				if ( $error == FALSE ) {
					$stock = $api->addStockEntry( $organizacija, json_encode($order) );
					if ( $stock != "ERROR" ) {
						echo "<span class='success'>Uspešno poslano v Minimax. Potrdite osnutek zaloge -> Poslovanje -> Zaloge</span>";
					} else {
						echo "<span class='error'>Prišlo je do napake pri pošiljanju v Minimax.</span>";
					}
					//printer( $order );
					//echo json_encode($order);
				} else {
					echo "<span class='error'>Zaradi manjkajočih podatkov ni bilo poslano v Minimax.</span>";
				}

			//printer($value);

		}



	}

	add_action( 'wp_ajax_sync_products', 'sync_products' );
	function sync_products() {
		global $wpdb;

		include_once MM_PLUGIN_DIR . '/api/api.php';
		$organizacija 	= get_option("organisationId", true);

		$api = new MMApi( 
			get_option("apiClientId", true),
			get_option("apiClientSecret", true),
			get_option("clientContextUserId", true),
			get_option("clientContextUserPass", true),
			get_option("apiRootUrl", true)
		);


		$produkti = get_woocommerce_product_list();
		$data = array();


		foreach ($produkti as $key => $value) {

			if ( $value->synced == TRUE ) {
				continue;
			}

$json = '{
"ItemId": '.$value->id.',
"Name": "'.$value->title.'",
"Code": "'.$value->sku.'",
"EANCode": null,
"Description": null,
"ItemType": "B",
"UnitOfMeasurement": null,
"VatRate": {
"ID": '.$value->vatRateID.',
"Name": null,
"ResourceUrl": null
},
"Price": '.$value->price.',
"RebatePercent": 0.0,
"Usage": "D",
"Currency": {
"ID": '.$value->currencyCode.',
"Name": null,
"ResourceUrl": null
},
"RevenueAccountDomestic": null,
"RevenueAccountEU": null,
"RevenueAccountOutsideEU": null,
"StocksAccount": null,
"RecordDtModified": "0001-01-01T00:00:00",
"RowVersion": null
}';

			$length = strlen( $json );

			$produkt = $api->addProduct( $organizacija, ($json), $length );

			if ( $produkt != "ERROR" ) {
				update_post_meta( $value->id, "minimaxSync", true );
				update_post_meta( $value->id, "minimaxID", $produkt );
			}

			
		}

		$produkt_postnina = get_option( "minimaxPostnina", "" );

		if ( $produkt_postnina == "" ) {

$json = '{
"ItemId": ,
"Name": "Poštnina - Woocommerce",
"Code": null,
"EANCode": null,
"Description": null,
"ItemType": "B",
"UnitOfMeasurement": null,
"VatRate": {
"ID": 30,
"Name": null,
"ResourceUrl": null
},
"Price": 0,
"RebatePercent": 0.0,
"Usage": "D",
"Currency": {
"ID": '.$value->currencyCode.',
"Name": null,
"ResourceUrl": null
},
"RevenueAccountDomestic": null,
"RevenueAccountEU": null,
"RevenueAccountOutsideEU": null,
"StocksAccount": null,
"RecordDtModified": "0001-01-01T00:00:00",
"RowVersion": null
}';

			$length = strlen( $json );

			$produkt = $api->addProduct( $organizacija, ($json), $length );
			update_option( "minimaxPostnina", $produkt );
		}



		$data["status"] = "success";
		$data["produkti"] = $produkti;

		echo json_encode($data);

		wp_die(); // this is required to terminate immediately and return a proper response

	}

	add_action( 'admin_enqueue_scripts', 'minimax_scripts' );
	function minimax_scripts() {

		wp_enqueue_script( 'ajax-script-minimax', plugins_url( 'admin/js/minimax.js', __FILE__ ), array('jquery') );

		// in JavaScript, object properties are accessed as ajax_object.ajax_url, ajax_object.we_value
		wp_localize_script( 'ajax-script-minimax', 'ajax_object_minimax',
	            array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );

	}


	// ********* Get all products and variations and sort alphbetically, return in array (title, sku, id)*******
	function get_woocommerce_product_list() {
		$full_product_list = array();
		$loop = new WP_Query( array( 'post_type' => array('product', 'product_variation'), 'posts_per_page' => -1 ) );
	 
		while ( $loop->have_posts() ) : $loop->the_post();
			$theid = get_the_ID();
			$product = new WC_Product($theid);

			if ( $product->post->post_type == "product_variation" ) {
				$product = new WC_Product_Variation( $theid );
				$product->variacija = true;
				$product->id = $product->variation_id;
			}

			$product->title = $product->get_title();
			$product->price = $product->get_price();
			$product->sku = $product->get_sku();

			$synced = get_post_meta( $product->id, "minimaxSync", true );


			$product->minimaxID = get_post_meta( $product->id, "minimaxID", true );
			$product->synced = $synced;
			$product->itemType = "B";


			$sync       	= get_post_meta( $theid, "minimaxAutomaticZaloga", true );
			$skladisce  	= get_post_meta( $theid, "minimaxSkladisce", true );
			$dobavitelj 	= get_post_meta( $theid, "minimaxDobavitelj", true );

			$product->syncZaloge = $sync;
			$product->skladisce  = $skladisce;
			$product->dobavitelj = $dobavitelj;


			$_tax = new WC_Tax();
	        $product_tax_class = $product->get_tax_class();
	        $tax =  $_tax->get_rates($product_tax_class);

	        $product->tax_class = $product_tax_class;
	        $product->tax = $tax;

	        if ( !empty( $tax ) && isset($tax[1]) ) {
	        	$product->vatRateID = findVAT( $tax[1]["rate"] );
	        } else {
	        	$product->vatRateID = findVAT( 0 );
	        }


	        $product->currencyCode = get_option( "valutaTrgovine", true );


			array_push($full_product_list, $product);
			
	    endwhile; 
	    wp_reset_query();
	    return $full_product_list;
	}

	function findVAT( $percent ) {

		$vat = get_option("minimaxVAT", array() );


		foreach ($vat as $key => $value) {
			
			if ( (float) $value["Percent"] == (float) sprintf("%.1f", $percent) ) {
				return $value["VatRateId"];
			}
		}

	}


	



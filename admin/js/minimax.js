jQuery(document).ready(function($) {


	jQuery(".js-sync-products").click( function(e){

		jQuery(".loader-mm").fadeIn();

		e.preventDefault();

		var data = {
			'action': 'sync_products'
		};
		// We can also pass the url value separately from ajaxurl for front end AJAX implementations
		jQuery.post(ajax_object_minimax.ajax_url, data, function(response) {

			$response = JSON.parse( response );

			console.log( $response );

			if ( $response["status"] == "success" ) {
				alert("Uspešno ste sinhnorizirali produkte v Minimax!  ");
				location.reload();
			} else {
				alert("Sinhnorizacija ni uspela. Poskusite znova!");
			}

			jQuery(".loader-mm").fadeOut();

		});

	});



	jQuery(".js-add-new-stock").click( function(e){


		e.preventDefault();

		if (  jQuery(".js-nova-zaloga").val() == "" || jQuery(".js-nova-zaloga").val() == "0" ) {
			alert("Vpišite število zaloge!");
			return;
		};

		jQuery("body").css("cursor", "wait");
		var data = {
			'action': 'single_product_add_stock',
			'qty': jQuery(".js-nova-zaloga").val(),
			'id': jQuery(".id-produkta").val()
		};
		// We can also pass the url value separately from ajaxurl for front end AJAX implementations
		jQuery.post(ajax_object_minimax.ajax_url, data, function(response) {

			$response = JSON.parse( response );

			console.log( $response );

			if ( $response["status"] == "success" ) {
				alert("Uspešno ste dodali prejem zaloge v Minimax. Potrebna je še potrditev prejema zaloge v Minimaxu.");
				jQuery(".id-produkta").val("");
			} else {
				alert("Prišlo je do napake! " + $response["explanation"] );
			}

			jQuery("body").css("cursor", "auto");

		});

	});





});
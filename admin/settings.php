<?php

	global $woocommerce;

	//print_r( WC()->countries->countries );

	global $plugin_page;

	////////////////////////////////////////////////////////////////////

	$organizacija 	= get_option("organisationId", true);

	////////////////////////////////////////////////////////////////////

	/// Get organizations
	include_once MM_PLUGIN_DIR . '/api/api.php';
	//GET api/user/orgs
	$apiMethodRelativeUrl = "currentuser/orgs";

	$api = new MMApi( 
		get_option("apiClientId", true),
		get_option("apiClientSecret", true),
		get_option("clientContextUserId", true),
		get_option("clientContextUserPass", true),
		get_option("apiRootUrl", true)
	);


	//printer($reports);

	/////$customer 		= $api->addCustomer( $organizacija );


	$command = null;
	if ( isset( $_REQUEST["command"] ) ) {
		$command = $_REQUEST["command"];
	}


	if ( $command == "shrani-podatke" ) {
		$tip = $_POST["Submit"];
		if ( "Shrani" == $tip ) {
			update_option("organisationId",  $_POST["organizationId"]);
			update_option("apiClientId",  $_POST["apiClientId"]);
			update_option("apiClientSecret",  $_POST["apiClientSecret"]);
			update_option("apiRootUrl",  $_POST["apiRootUrl"]);
			update_option("clientContextUserId",  $_POST["clientContextUserId"]);
			update_option("clientContextUserPass",  $_POST["clientContextUserPass"]);



			/// Currencies sync
			$currencies = $api->getAllCurrencies( $organizacija );
			if ( isset($currencies["Rows"]) ) {
				update_option( "minimaxCurrencies", $currencies["Rows"] );
			}

			/// Warehouses sync
			$warehouses = $api->getAllWarehouses( $organizacija );
			if ( isset($warehouses["Rows"]) ) {
				update_option( "minimaxWarehouses", $warehouses["Rows"] );
			}

			/// Customers sync
			$customers = $api->getAllCustomers( $organizacija );
			if ( isset($customers["Rows"]) ) {
				update_option( "minimaxCustomers", $customers["Rows"] );
			}

			/// Countries sync
			$countries = $api->getAllCountries( $organizacija );
			if ( isset($countries["Rows"]) ) {
				update_option( "minimaxCountries", $countries["Rows"] );
			}

			/// VAT sync
			$vat = $api->getAllVAT( $organizacija );
			if ( isset($vat["Rows"]) ) {
				update_option( "minimaxVAT", $vat["Rows"] );
			}



		} 
	}


	if ( $command == "shrani-nastavitve" ) {
		$tip = $_POST["Submit"];
		if ( "Shrani" == $tip ) {


			update_option("DeliveryNoteReportTemplate",  $_POST["DeliveryNoteReportTemplate"]);
			update_option("valutaTrgovine",  $_POST["valutaTrgovine"]);
			update_option("reportInvoice",  $_POST["reportInvoice"]);
			update_option("reportOrder",  $_POST["reportOrder"]);

			if ( isset($_POST["syncOrders"]) ) {
				update_option("syncOrders",  $_POST["syncOrders"]);
			}			
			if ( isset($_POST["syncZaloga"]) ) {
				update_option("syncZaloga",  $_POST["syncZaloga"]);
			}

		} 
	}



	////////////////////////////////////////////////////////////////////

	$organizacija 	= get_option("organisationId", true);
	$skladisca 		= get_option("minimaxWarehouses", array() );
	$stranke 		= get_option("minimaxCustomers", array() );
	$drzave 		= get_option("minimaxCountries", array() );
	$valute 		= get_option("minimaxCurrencies", array() );
	$stopnje 		= get_option("minimaxVAT", array() );

	//printer($valute);

	////////////////////////////////////////////////////////////////////


	$organizations  = $api->getOrganizations( );
	$reports 		= $api->getAllReports( $organizacija )["Rows"];
?>

<div class="loader-mm" style="position:fixed; top: 50%; left: 50%; display:none; z-index: 1000;">
	<img src="<?php echo plugin_dir_url( __FILE__ ) . 'img/';?>ajax-loader.gif" alt="">
</div>

<div class="wrap">
	<h2>Nastavitve Woocommerce -> Minimax</h2>

	<hr>

	<div id="dashboard-widgets-wrap">
	    <div id="dashboard-widgets" class="metabox-holder">

	        <div class="postbox-container minimax">
	            <div id="normal-sortables" class="ui-sortable meta-box-sortable">
	                <!-- BOXES -->
	                <div class="postbox" style="padding: 25px;">

						<h4>Dostop do vašega Minimax računa:</h4>

						<form method="post" action="<?php echo esc_url( menu_page_url( 'mm-module', false ) ); ?>">
						    <table class="form-table">
								<input type="hidden" name="command" value="shrani-podatke">
						    	
						        <tr valign="top">
						        <th scope="row">ID Organizacije</th>
						        <td>
						        	<select name="organizationId" id=""  style="width: 100%;">
						        		<option value="/">/</option>
						        		<?php
						        			foreach ($organizations["Rows"] as $key => $value) {
						        		?>
											<option <?php if( $value["Organisation"]["ID"] == get_option( "organisationId" ) ) { echo "selected='selected' "; }; ?>value="<?php echo $value["Organisation"]["ID"];?>"><?php echo $value["Organisation"]["Name"];?></option>
						        		<?php
						        			}
						        		?>
						        	</select>
						        </td>
						        </tr>
						    							    	
						        <tr valign="top">
						        <th scope="row">API ID</th>
						        <td><input type="text"  style="width: 100%;" name="apiClientId" value="<?php echo esc_attr( get_option('apiClientId') ); ?>" /></td>
						        </tr>
						    	
						        <tr valign="top">
						        <th scope="row">API Geslo</th>
						        <td><input type="text"  style="width: 100%;" name="apiClientSecret" value="<?php echo esc_attr( get_option('apiClientSecret') ); ?>" /></td>
						        </tr>
						         
						    	
						        <tr valign="top">
						        <th scope="row">API URL naslov (domena)</th>
						        <td><input type="text" style="width: 100%;" name="apiRootUrl" value="<?php echo esc_attr( get_option('apiRootUrl', 'https://moj.minimax.si/demo/si/') ); ?>" /></td>
						        </tr>
						         
						    	
						        <tr valign="top">
						        <th scope="row">Uporabniško ime</th>
						        <td><input type="text" style="width: 100%;" name="clientContextUserId" value="<?php echo esc_attr( get_option('clientContextUserId') ); ?>" /></td>
						        </tr>
						         			    	
						        <tr valign="top">
						        <th scope="row">Uporabniško geslo</th>
						        <td><input type="text" style="width: 100%;" name="clientContextUserPass" value="<?php echo esc_attr( get_option('clientContextUserPass') ); ?>" /></td>
						        </tr>
						         
	

						    </table>

								<button type="submit" name="Submit" class="button button-primary" value="Shrani">Shrani spremembe in pridobi podatke</button>

						    </p>

						</form>

	                </div>
	            </div>
	        </div>




	        <div class="postbox-container minimax">
	            <div id="normal-sortables" class="ui-sortable meta-box-sortable">
	                <!-- BOXES -->
	                <div class="postbox" style="padding: 25px;">

						<h4>Nastavitve</h4>

						<form method="post" action="<?php echo esc_url( menu_page_url( 'mm-module', false ) ); ?>">
						    <table class="form-table">
								<input type="hidden" name="command" value="shrani-nastavitve">

						        <tr valign="top">
						        <th scope="row">Valuta trgovine</th>
						        <td>
						        	<select name="valutaTrgovine" id="" style="width: 100%;">
						        		<option value="/">/</option>
						        		<?php
						        			foreach ($valute as $key => $value) {
						        		?>
											<option <?php if( $value["CurrencyId"] == get_option( "valutaTrgovine" ) ) { echo "selected='selected' "; }; ?>value="<?php echo $value["CurrencyId"];?>"><?php echo $value["Name"];?></option>
						        		<?php
						        			}
						        		?>
						        	</select>
						        </td>
						        </tr>	

						    	
						        <tr valign="top">
						        <th scope="row">Izpis za račun</th>
						        <td>
						        	<select name="reportInvoice" id="" style="width: 100%;">
						        		<option value="/">/</option>
						        		<?php
						        			foreach ($reports as $key => $value) {
						        		?>
											<option <?php if( $value["ReportTemplateId"] == get_option( "reportInvoice" ) ) { echo "selected='selected' "; }; ?>value="<?php echo $value["ReportTemplateId"];?>"><?php echo $value["Name"];?></option>
						        		<?php
						        			}
						        		?>
						        	</select>
						        </td>
						        </tr>

						        <tr valign="top">
						        <th scope="row">Izpis za dobavnico</th>
						        <td>
						        	<select name="DeliveryNoteReportTemplate" id="" style="width: 100%;">
						        		<option value="/">/</option>
						        		<?php
						        			foreach ($reports as $key => $value) {
						        				if ( $value["DisplayType"] != "DO" ) {
						        					continue;
						        				}
						        		?>
											<option <?php if( $value["ReportTemplateId"] == get_option( "DeliveryNoteReportTemplate" ) ) { echo "selected='selected' "; }; ?>value="<?php echo $value["ReportTemplateId"];?>"><?php echo $value["Name"];?></option>
						        		<?php
						        			}
						        		?>
						        	</select>
						        </td>
						        </tr>
						    	

						        <tr valign="top">
						        <th scope="row">Izpis za naročilo</th>
						        <td>
						        	<select name="reportOrder" id="" style="width: 100%;">
						        		<option value="/">/</option>
						        		<?php
						        			foreach ($reports as $key => $value) {
						        		?>
											<option <?php if( $value["ReportTemplateId"] == get_option( "reportOrder" ) ) { echo "selected='selected' "; }; ?>value="<?php echo $value["ReportTemplateId"];?>"><?php echo $value["Name"];?></option>
						        		<?php
						        			}
						        		?>
						        	</select>
						        </td>
						        </tr>
						    	
						        <tr valign="top">
						        <th scope="row">Avtomatsko sinhnoriziraj prejeta naročila</th>
						        <td><input type="checkbox" name="syncOrders" value="1" <?php if( get_option( "syncOrders" ) ) { echo "checked='checked' "; }; ?>/></td>
						        </tr>		

						        <tr valign="top">
						        <th scope="row">Avtomatsko izdaj zalogo ob izdaji računa</th>
						        <td><input type="checkbox" name="syncZaloga" value="1" <?php if( get_option( "syncZaloga" ) ) { echo "checked='checked' "; }; ?>/></td>
						        </tr>	

						        <tr valign="top">
						        <td colspan="2">
									<button type="submit" name="Submit" class="button button-primary" value="Shrani">Shrani</button>
						        </td>
						        </tr>

						        <tr valign="top">
						        <td colspan="2">
						        	<hr>
						        </td>
						        </tr>
						         
						         						    	
						        <tr valign="top">
						        <th scope="row">Strank v Minimaxu</th>
						        <td>
						        	<?php echo count($stranke);?>
						        </td>
						        </tr>		

						        <tr valign="top">
						        <th scope="row">Skladišča v Minimaxu</th>
						        <td>
						        	<?php echo count($skladisca);?>
						        </td>
						        </tr>
						         								         						    	
						        <tr valign="top">
						        <th scope="row">Države v Minimaxu</th>
						        <td>
						        	<?php echo count($drzave);?>
						        </td>
						        </tr>
						         						    	
						        <tr valign="top">
						        <th scope="row">Valute v Minimaxu</th>
						        <td>
						        	<?php echo count($valute);?>
						        </td>
						        </tr>
						         
						    							         						    	
						        <tr valign="top">
						        <th scope="row">Davčne stopnje v Minimaxu</th>
						        <td>
						        	<?php echo count($stopnje);?>
						        </td>
						        </tr>
						         
						    	
						         
	

						    </table>



						</form>

						<br>

						<hr>

						<br>

						<h4>Operacije - <strong><small style="color: red;">Pazljivo ravnanje!</small></strong> </h4> 

						<small>Produkti ki so že bili sinhnorizirani bodo ignorirani!</small>
						<br>
						<a href="<?php echo wp_nonce_url( admin_url( 'admin-ajax.php?action=sync_products&module=true' ), 'sync_products' );?>" class="js-sync-products button button-info">Sinhnoriziraj produkte v Minimax</a>
						
						<br>
						<br>
						<hr>
						<br>

						<small>Poslan bo trenuten status zaloge iz WooCommerce v Minimax (kot prejem zaloge). Produkt mora imeti nastavljene parametre za zalogo (skladišče, dobavitelja,..) !</small>
						<br>
						<a href="<?php echo wp_nonce_url( admin_url( 'admin-ajax.php?action=sync_zaloga_first&module=true' ), 'sync_zaloga_first' );?>" class="button button-info">Sinhnoriziraj zalogo v Minimax</a>

	                </div>
	            </div>
	        </div>






	    </div>
	</div>


</div>
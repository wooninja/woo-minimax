<?php

class MMApi {


    public $token;

    public function __construct($client_id, $client_secret, $username, $password, $url)
    {
        $this->client_id        = $client_id;
        $this->client_secret    = $client_secret;
        $this->username         = $username;
        $this->password         = $password;
        $this->url              = $url;

        if ( strlen($url) < 5 ) {
            return;
        }

        $this->getToken();

    }

    public function getToken() {

        $params = array(
            'client_id'        => $this->client_id ,
            'client_secret'    => $this->client_secret ,
            'grant_type'       => "password" ,
            'username'         => $this->username ,
            'password'         => $this->password ,
            'scope'            => 'minimax.si'
        );

        $request = array(
            'http' => array(
                'method'        => 'POST',
                'header'        => array(
                    'Content-type: application/x-www-form-urlencoded',
                ),
                'content'        => http_build_query($params),
                'timeout'        => 15,
            )
        );

        $response = file_get_contents( $this->url . 'aut/oauth20/token', false, stream_context_create($request) );

        if (!$response ) {
            $this->token = false;
            return false;
        }
        $token = json_decode($response);


        $this->token = $token;
        return $this->token;
        
    }

    public function getOrganizations() {

        if ( !$this->token ) {
            return array();
        }

        $request = array(
            'http' => array(
                'method'        => 'GET',
                'header'        => 'Authorization: Bearer ' . $this->token->access_token,
                'timeout'        => 10,
            )
        );
        if (!$response = file_get_contents( $this->url . 'api/api/currentuser/orgs', false, stream_context_create($request))) {
            die('orgs error');
        }
        $orgs = json_decode($response, true);
        return $orgs;

    }



    /////
    /// REPORTS
    /////

    public function getAllReports( $organisationId ) {

        if ( $organisationId == "1" OR $organisationId == "" OR $organisationId == null ) {
            return;
        }

        $params = array(
            'CurrentPage'    => "5",
            'PageSize'    => "500",
        );

        $request = array(
            'http' => array(
                'method'        => 'GET',
                'header'        => array( 'Content-type: application/json; charset=utf-8', 'Authorization: Bearer ' . $this->token->access_token ),
                'timeout'        => 15,
                'content'        => http_build_query($params),
            )
        );

        if (!$response = file_get_contents( $this->url . 'api/api/orgs/'.$organisationId.'/report-templates?PageSize=500', false, stream_context_create($request))) {
            return array();
        }

        $orgs = json_decode($response, true);

        return $orgs;

    }



    /////
    /// CURRENCIES
    /////

    public function getAllCurrencies( $organisationId ) {

        if ( $organisationId == "1" OR $organisationId == "" OR $organisationId == null ) {
            return;
        }

        $params = array(
            'CurrentPage'    => "5",
            'PageSize'    => "500",
        );

        $request = array(
            'http' => array(
                'method'        => 'GET',
                'header'        => array( 'Content-type: application/json; charset=utf-8', 'Authorization: Bearer ' . $this->token->access_token ),
                'timeout'        => 15,
                'content'        => http_build_query($params),
            )
        );

        if (!$response = file_get_contents( $this->url . 'api/api/orgs/'.$organisationId.'/currencies?PageSize=500', false, stream_context_create($request))) {
            return array();
        }

        $orgs = json_decode($response, true);

        return $orgs;

    }



    /////
    /// COUNTRIES
    /////

    public function getAllCountries( $organisationId ) {

        if ( $organisationId == "1" OR $organisationId == "" OR $organisationId == null ) {
            return;
        }

        $params = array(
            'CurrentPage'    => "5",
            'PageSize'    => "500",
        );

        $request = array(
            'http' => array(
                'method'        => 'GET',
                'header'        => array( 'Content-type: application/json; charset=utf-8', 'Authorization: Bearer ' . $this->token->access_token ),
                'timeout'        => 15,
                'content'        => http_build_query($params),
            )
        );

        if (!$response = file_get_contents( $this->url . 'api/api/orgs/'.$organisationId.'/countries?PageSize=500', false, stream_context_create($request))) {
            return array();
        }

        $orgs = json_decode($response, true);

        return $orgs;

    }



    /////
    /// VAT
    /////

    public function getAllVAT( $organisationId ) {

        if ( $organisationId == "1" OR $organisationId == "" OR $organisationId == null ) {
            return;
        }

        $params = array(
            'CurrentPage'    => "5",
            'PageSize'    => "500",
        );

        $request = array(
            'http' => array(
                'method'        => 'GET',
                'header'        => array( 'Content-type: application/json; charset=utf-8', 'Authorization: Bearer ' . $this->token->access_token ),
                'timeout'        => 15,
                'content'        => http_build_query($params),
            )
        );

        if (!$response = file_get_contents( $this->url . 'api/api/orgs/'.$organisationId.'/vatrates?PageSize=500', false, stream_context_create($request))) {
            return array();
        }

        $orgs = json_decode($response, true);

        return $orgs;

    }



    /////
    /// INVOICE
    /////

    public function addInvoice( $organisationId, $data ) {

        if ( $organisationId == "1" OR $organisationId == "" OR $organisationId == null ) {
            return;
        }

        $request = array(
            'http' => array(
                'method'        => 'POST',
                'header'        => array( 'content-length: '.strlen($data), 'Content-type: application/json; charset=utf-8', 'Accept: application/json; charset=utf-8', 'Authorization: Bearer ' . $this->token->access_token ),
                'timeout'        => 15,
                'content'        => ($data),
            )
        );

        if (!$response = @file_get_contents( $this->url . 'api/api/orgs/'.$organisationId.'/issuedinvoices', false, stream_context_create($request))) {
            return "ERROR";
        }

        $url = $http_response_header[5];
        $parts = parse_url($url);
        parse_str($parts['query'], $query);



        $orgs = json_decode($response, true);

        return $query["id"];

    }

    public function getInvoice( $organisationId, $ID ) {

        if ( $organisationId == "1" OR $organisationId == "" OR $organisationId == null ) {
            return;
        }

        $request = array(
            'http' => array(
                'method'        => 'GET',
                'header'        => array( 'Content-type: application/json; charset=utf-8', 'Accept: application/json; charset=utf-8', 'Authorization: Bearer ' . $this->token->access_token ),
                'timeout'        => 15,
            )
        );

        if (!$response = @file_get_contents( $this->url . 'api/api/orgs/'.$organisationId.'/issuedinvoices/' . $ID, false, stream_context_create($request))) {
            return "ERROR";
        }

        $url = $http_response_header[5];
        $parts = parse_url($url);
        parse_str($parts['query'], $query);



        $orgs = json_decode($response, true);

        return json_decode($response);

    }

    public function invoiceAction( $organisationId, $data ) {

        if ( $organisationId == "1" OR $organisationId == "" OR $organisationId == null ) {
            return;
        }


        $request = array(
            'http' => array(
                'method'        => 'PUT',
                'header'        => array( 'content-length: 0', 'Content-type: application/json; charset=utf-8', 'Accept: application/json; charset=utf-8', 'Authorization: Bearer ' . $this->token->access_token ),
                'timeout'        => 15,
            )
        );

        if (!$response = @file_get_contents( $this->url . 'api/api/orgs/'.$organisationId.'/issuedinvoices/'.$data["id"]."/actions/".$data["action"]."?rowVersion=", false, stream_context_create($request))) {
            return "ERROR";
        }

        $url = $http_response_header;

        return $url;

    }

    /////
    /// ORDERS
    /////

    public function addOrder( $organisationId, $data ) {

        if ( $organisationId == "1" OR $organisationId == "" OR $organisationId == null ) {
            return;
        }

        $request = array(
            'http' => array(
                'method'        => 'POST',
                'header'        => array( 'content-length: '.strlen($data), 'Content-type: application/json; charset=utf-8', 'Accept: application/json; charset=utf-8', 'Authorization: Bearer ' . $this->token->access_token ),
                'timeout'        => 15,
                'content'        => ($data),
            )
        );

        if (!$response = @file_get_contents( $this->url . 'api/api/orgs/'.$organisationId.'/orders', false, stream_context_create($request))) {
            return "ERROR";
        }

        $url = $http_response_header[5];
        $parts = parse_url($url);
        parse_str($parts['query'], $query);



        $orgs = json_decode($response, true);

        return $query["id"];

    }

    /////
    /// WAREHOUSES
    /////


    public function getAllWarehouses( $organisationId ) {

        if ( $organisationId == "1" OR $organisationId == "" OR $organisationId == null ) {
            return;
        }

        $params = array(
            'CurrentPage'    => "5",
            'PageSize'    => "100000",
        );

        $request = array(
            'http' => array(
                'method'        => 'GET',
                'header'        => array( 'Content-type: application/json; charset=utf-8', 'Authorization: Bearer ' . $this->token->access_token ),
                'timeout'        => 15,
                'content'        => http_build_query($params),
            )
        );

        if (!$response = file_get_contents( $this->url . 'api/api/orgs/'.$organisationId.'/warehouses?PageSize=100000', false, stream_context_create($request))) {
            return array();
        }

        $orgs = json_decode($response, true);

        return $orgs;

    }

    /////
    /// CUSTOMERS
    /////


    public function getAllCustomers( $organisationId ) {

        if ( $organisationId == "1" OR $organisationId == "" OR $organisationId == null ) {
            return;
        }

        $params = array(
            'CurrentPage'    => "5",
            'PageSize'    => "100000",
        );

        $request = array(
            'http' => array(
                'method'        => 'GET',
                'header'        => array( 'Content-type: application/json; charset=utf-8', 'Authorization: Bearer ' . $this->token->access_token ),
                'timeout'        => 15,
                'content'        => http_build_query($params),
            )
        );

        if (!$response = file_get_contents( $this->url . 'api/api/orgs/'.$organisationId.'/customers?PageSize=100000', false, stream_context_create($request))) {
            return array();
        }

        $orgs = json_decode($response, true);

        return $orgs;

    }



    public function addCustomer( $organisationId, $params ) {

        if ( $organisationId == "1" OR $organisationId == "" OR $organisationId == null ) {
            return;
        }


        $request = array(
            'http' => array(
                'method'        => 'POST',
                'header'        => array( 'content-length: '.strlen($params), 'Content-type: application/json; charset=utf-8', 'Accept: application/json; charset=utf-8', 'Authorization: Bearer ' . $this->token->access_token ),
                'timeout'        => 15,
                'content'        => ($params),
            )
        );

        if (!$response = file_get_contents( $this->url . 'api/api/orgs/'.$organisationId.'/customers', false, stream_context_create($request))) {
            return "ERROR";
        }


        $url = $http_response_header[5];
        $parts = parse_url($url);
        parse_str($parts['query'], $query);



        $orgs = json_decode($response, true);

        return $query["id"];

    }

   /////
    /// PRODUCTS
    /////

    public function addProduct( $organisationId, $data, $length ) {

        if ( $organisationId == "1" OR $organisationId == "" OR $organisationId == null ) {
            return;
        }


        $request = array(
            'http' => array(
                'method'        => 'POST',
                'header'        => array( 'content-length: '.$length, 'Content-type: application/json; charset=utf-8', 'Accept: application/json; charset=utf-8', 'Authorization: Bearer ' . $this->token->access_token ),
                'timeout'        => 15,
                'content'        => ($data),
            )
        );

        if (!$response = @file_get_contents( $this->url . 'api/api/orgs/'.$organisationId.'/items', false, stream_context_create($request))) {
            return "ERROR";
        }


        $url = $http_response_header[5];
        $parts = parse_url($url);
        parse_str($parts['query'], $query);



        $orgs = json_decode($response, true);

        return $query["id"];

    }

    /////
    /// STOCK
    /////

    public function getAllStocks( $organisationId ) {

        if ( $organisationId == "1" OR $organisationId == "" OR $organisationId == null ) {
            return;
        }


        $request = array(
            'http' => array(
                'method'        => 'GET',
                'header'        => array( 'Content-type: application/json; charset=utf-8', 'Accept: application/json; charset=utf-8', 'Authorization: Bearer ' . $this->token->access_token ),
                'timeout'        => 15,
            )
        );

        if (!$response = @file_get_contents( $this->url . 'api/api/orgs/'.$organisationId.'/stocks?PageSize=100000', false, stream_context_create($request))) {
            return array();
        }

        $orgs = json_decode($response, true);

        return $orgs;

    }


    public function stockAction( $organisationId, $data ) {

        if ( $organisationId == "1" OR $organisationId == "" OR $organisationId == null ) {
            return;
        }


        $request = array(
            'http' => array(
                'method'        => 'PUT',
                'header'        => array( 'content-length: 0', 'Content-type: application/json; charset=utf-8', 'Accept: application/json; charset=utf-8', 'Authorization: Bearer ' . $this->token->access_token ),
                'timeout'        => 25,
            )
        );

        if (!$response = @file_get_contents( $this->url . 'api/api/orgs/'.$organisationId.'/stockentry/'.$data["id"]."/actions/".$data["action"]."?rowVersion=", false, stream_context_create($request))) {
            return "ERROR";
        }

        $url = $http_response_header;

        return $url;

    }

    public function addStockEntry( $organisationId, $data ) {

        if ( $organisationId == "1" OR $organisationId == "" OR $organisationId == null ) {
            return;
        }


        $request = array(
            'http' => array(
                'method'        => 'POST',
                'header'        => array( 'content-length: '. strlen( $data ) , 'Content-type: application/json; charset=utf-8', 'Accept: application/json; charset=utf-8', 'Authorization: Bearer ' . $this->token->access_token ),
                'timeout'        => 15,
                'content'        => ($data),
            )
        );

        if (!$response = @file_get_contents( $this->url . 'api/api/orgs/'.$organisationId.'/stockentry', false, stream_context_create($request))) {
            return "ERROR";
        }


        $url = $http_response_header[5];
        $parts = parse_url($url);
        parse_str($parts['query'], $query);



        $orgs = json_decode($response, true);

        return $query["id"];

    }

}